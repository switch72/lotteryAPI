﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using lottery_logic;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace lottery.API
{
    [Route("api")]
    public class LotteryController : Controller
    {

        [HttpGet("{ticketnumber}")]
        public IActionResult PrintTicket(int ticketnumber)
        {

            ticket checkticket;
            checkticket = LotteryDataStore.ticketlist.Find(x => x.TicketNumber() == ticketnumber);
            if (checkticket != null)
            {
                return new JsonResult(new List<object>()
                    {
                    new {Number = checkticket.TicketNumber()},
                    new {Powerplay = checkticket.PowerPlay()},
                    new {Date = checkticket.TicketDate()},
                    new {Picks = checkticket.ReadNumbers()}
                    });
            }
            else
            {
                return new JsonResult("invalid ticketnumber");
            }
        }

        [HttpPost("newticket/quickpick/{powerplay}")]
        public IActionResult NewTicketQuickPick(bool powerplay)
        {
            ticket newticket = new ticket();
            try
            {
                newticket.QuickPick(powerplay);
                LotteryDataStore.ticketlist.Add(newticket);
                return new JsonResult(new List<object>()
                    {"Ticket number", newticket.TicketNumber()," created" });
            }
            catch (ArgumentException error) { return new JsonResult(error.Message); }

        }

        [HttpPost("newticket/picksix/{powerplay}")]
        public IActionResult NewTicketPickSix(bool powerplay, [FromBody]List<int> picklist)
        {
            ticket newticket = new ticket();
            try
            {
                newticket.PickSix(picklist,powerplay);
                LotteryDataStore.ticketlist.Add(newticket);
                return new JsonResult(new List<object>()
                    {"Ticket number", newticket.TicketNumber()," created" });
            }
            catch (ArgumentException error) { return new JsonResult(error.Message); }

        }

        [HttpPost("drawing")]
        public IActionResult Drawing()
        {
            
            try
            {
                LotteryDataStore.winningticket.QuickPick(false);
                Random pickerpowerplay = new Random();
                LotteryDataStore.powerplaymultiplier = pickerpowerplay.Next(2, 10);
                return new JsonResult("Drawing Complete");

            }
            catch (ArgumentException error) { return new JsonResult(error.Message); }

        }

        [HttpGet("checkticket/{ticketnumber}")]
        public IActionResult Checkticket(int ticketnumber)
        {

            ticket checkticket;
            checkticket = LotteryDataStore.ticketlist.Find(x => x.TicketNumber() == ticketnumber);
            if (checkticket != null)
            {
                if (LotteryDataStore.winningticket.ReadNumbers().Any())
                {
                    int whitematches;
                    int payout;
                    bool powerballmatch;
                    whitematches = Enumerable.Intersect(checkticket.ReadNumbers().GetRange(0, 4), LotteryDataStore.winningticket.ReadNumbers().GetRange(0, 4)).Count();
                    powerballmatch = (checkticket.ReadNumbers()[5] == LotteryDataStore.winningticket.ReadNumbers()[5]);

                    if (powerballmatch)
                    {
                        switch (whitematches)
                        {
                            case 0:
                                payout = 4;
                                break;
                            case 1:
                                payout = 4;
                                break;
                            case 2:
                                payout = 7;
                                break;
                            case 3:
                                payout = 100;
                                break;
                            case 4:
                                payout = 50000;
                                break;
                            case 5:
                                payout = LotteryDataStore.jackpot;
                                break;
                            default:
                                payout = 0;
                                break;

                        }
                    }
                    else
                    {
                        switch (whitematches)
                        {
                            case 3:
                                payout = 7;
                                break;
                            case 4:
                                payout = 100;
                                break;
                            case 5:
                                payout = 1000000;
                                break;
                            default:
                                payout = 0;
                                break;
                        }
                    }
                    if (checkticket.PowerPlay()) { if (payout < 150000000) payout = payout * LotteryDataStore.powerplaymultiplier; }

                    return new JsonResult(new List<object>()
                    {
                    new {Number = checkticket.TicketNumber()},
                    new {Powerplay = checkticket.PowerPlay()},
                    new {Date = checkticket.TicketDate()},
                    new {Picks = checkticket.ReadNumbers()},
                    new {WinningNumber = LotteryDataStore.winningticket.ReadNumbers()},
                    new {WhiteBallMatches = whitematches},
                    new {PowerBallMatch = powerballmatch},
                    new {Payout = payout}
                    });
                }
                else
                {
                    return new JsonResult("Drawing hasn't happened yet. Check back later.");
                }
            }
            else
            {
                return new JsonResult("invalid ticketnumber");
            }

        }

    }
}
